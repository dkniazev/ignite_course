package com.epam.ignite;

import com.epam.ignite.data.IpData;

/**
 * @author Dmitrii_Kniazev
 * @since 01/24/2017
 */
final class BaseReport {
    static String ipData2String(final IpData data) {
        return String.format("Ip:%s; Bytes:%d; AvgBytes:%.2f",
                data.getIp(),
                data.getBytes(),
                (double) data.getBytes() / data.getCounter());
    }

    static IpData getMax(final IpData v1, final IpData v2) {
        if (v1.getBytes() > v2.getBytes()) {
            return v1;
        } else {
            return v2;
        }
    }

    static IpData getMin(final IpData v1, final IpData v2) {
        if (v1.getBytes() < v2.getBytes()) {
            return v1;
        } else {
            return v2;
        }
    }
}
