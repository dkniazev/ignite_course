package com.epam.ignite.data;

import java.io.Serializable;

/**
 * @author Dmitrii_Kniazev
 * @since 23.11.16
 */
public enum WebClients implements Serializable {
    MOZILLA,
    IE,
    CHROME,
    SAFARI,
    OTHER
}
