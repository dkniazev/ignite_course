package com.epam.ignite.data;

import lombok.Getter;
import lombok.Setter;
import org.apache.ignite.binary.BinaryObject;
import org.apache.ignite.cache.query.annotations.QuerySqlField;
import org.apache.ignite.internal.binary.BinaryEnumObjectImpl;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Dmitry
 * @since 22.01.2017
 */
public final class IpData implements Serializable {
    @Getter
    @Setter
    @QuerySqlField(index = true)
    private String ip;

    @Getter
    @Setter
    @QuerySqlField
    private long bytes;

    @Getter
    @Setter
    @QuerySqlField
    private int counter = 1;

    @Getter
    @Setter
    private Set<WebClients> clients;

    /**
     * Default constructor.
     */
    public IpData() {
        // No-op.
    }

    @Override
    public String toString() {
        final String clientsStr = clients.stream()
                .map(Enum::toString)
                .collect(Collectors.joining(","));
        return String.format("IpData [ip=%s; bytes=%d; counter=%d; clients=%s]",
                ip,
                bytes,
                counter,
                clientsStr
        );
    }

    public static IpData extractFromBinary(BinaryObject bo) {
        final IpData data = new IpData();
        data.setIp(bo.field("ip"));
        data.setBytes(bo.field("bytes"));
        data.setCounter(bo.field("counter"));
        final Set<WebClients> clients = new HashSet<>();
        {
            final Set raw = bo.field("clients");
            for (Object o : raw) {
                if (o instanceof BinaryEnumObjectImpl) {
                    BinaryEnumObjectImpl beo = (BinaryEnumObjectImpl) o;
                    clients.add(beo.deserialize());
                } else {
                    clients.add((WebClients) o);
                }
            }
        }
        data.setClients(clients);
        return data;
    }
}
