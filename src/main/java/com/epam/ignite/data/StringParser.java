package com.epam.ignite.data;

import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;

import java.util.Collections;

/**
 * @author Dmitrii_Kniazev
 * @since 01/23/2017
 */
public final class StringParser {
    private static final UserAgentStringParser USER_AGENT_PARSER =
            UADetectorServiceFactory.getResourceModuleParser();

    public static IpData parseString2Data(final String inputStr) {
        final String[] splitTest = inputStr.split(" \"");
        final String ip = getIp(splitTest[0]);
        final int byteSize = getByteSize(splitTest[1]);
        final WebClients clientType = getClientType(splitTest[3]);
        final IpData ipData = new IpData();
        ipData.setIp(ip);
        ipData.setBytes(byteSize);
        ipData.setClients(Collections.singleton(clientType));
        return ipData;
    }

    /**
     * Find ip in input string<br/>
     * Example: "ip1 - - [24/Apr/2011:04:06:01 -0400]"
     * Return: "ip1"
     *
     * @param ipStr sting with ip
     * @return ip
     */
    private static String getIp(final String ipStr) {
        return ipStr.split(" ", 2)[0];
    }

    /**
     * Find size of transferred bytes in input string<br/>
     * Example: "GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028"
     * Return: "40028"
     * Example: "GET /sunFAQ/faq_framebuffer/framebuffer.html HTTP/1.1" 304 -"
     * Return: "0"
     *
     * @param requestStr string with
     * @return number of byte or 0 if not found
     */
    private static int getByteSize(final String requestStr) {
        final String byteStr = requestStr.split(" ", 5)[4];
        if (byteStr.matches("\\d+")) {
            return Integer.parseInt(byteStr);
        }
        return 0;
    }

    /**
     * Try find web client if possible.
     *
     * @param clientStr sting with client
     * @return {@code IE} or {@code MOZILLA} or {@code OTHER} value
     */
    private static WebClients getClientType(final String clientStr) {
        final ReadableUserAgent agent = USER_AGENT_PARSER.parse(clientStr);
        switch (agent.getFamily()) {
            case IE:
            case IE_MOBILE:
                return WebClients.IE;
            case FIREFOX:
            case MOBILE_FIREFOX:
            case FIREFOX_BONECHO:
            case FIREFOX_GRANPARADISO:
            case FIREFOX_LORENTZ:
            case FIREFOX_MINEFIELD:
            case FIREFOX_NAMOROKA:
            case FIREFOX_SHIRETOKO:
                return WebClients.MOZILLA;
            case CHROME:
            case CHROME_MOBILE:
                return WebClients.CHROME;
            case SAFARI:
            case MOBILE_SAFARI:
                return WebClients.SAFARI;
            default:
                return WebClients.OTHER;
        }
    }
}
