package com.epam.ignite;

import com.epam.ignite.data.IpData;
import com.epam.ignite.data.WebClients;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitrii_Kniazev
 * @since 01/23/2017
 */
public class IgniteDataReportSql extends IgniteConnector {
    private static final Logger LOG =
            LoggerFactory.getLogger(IgniteDataReport.class);

    public static void main(String[] args) {
        final IgniteDataReportSql dataImporter = new IgniteDataReportSql();
        try (final Ignite ignite = dataImporter.getIgnite()) {
            final IgniteCache<String, IpData> ipDataCache =
                    ignite.getOrCreateCache(CACHE_NAME);

            final SqlFieldsQuery query = getSqlQuery();
            try (final QueryCursor<List<?>> cursor = ipDataCache.query(query)) {

                IpData minByte = new IpData();
                minByte.setBytes(Long.MAX_VALUE);
                IpData maxByte = new IpData();
                maxByte.setBytes(Long.MIN_VALUE);
                final Map<WebClients, Integer> clientMap = new HashMap<>(10);

                for (final List<?> objects : cursor) {
                    final IpData data = (IpData) objects.get(0);
                    System.out.println(BaseReport.ipData2String(data));
                    minByte = BaseReport.getMin(minByte, data);
                    maxByte = BaseReport.getMax(maxByte, data);
                    for (WebClients webClient : data.getClients()) {
                        final Integer num = clientMap
                                .compute(webClient, (k, v) -> v == null ? 0 : (v + 1));
                    }
                }
                System.out.println("\nMax bytes:\n" + BaseReport.ipData2String(maxByte));
                System.out.println("\nMin bytes:\n" + BaseReport.ipData2String(minByte));
                System.out.println();
                clientMap.forEach((k, v) -> System.out.printf("%s : %d\n", k, v));
            }
        }
    }

    private static SqlFieldsQuery getSqlQuery() {
        final String sql = "SELECT _val  FROM IpData";
        return new SqlFieldsQuery(sql);
    }
}
