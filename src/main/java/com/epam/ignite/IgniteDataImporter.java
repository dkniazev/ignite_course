package com.epam.ignite;

import com.epam.ignite.data.IpData;
import com.epam.ignite.data.StringParser;
import com.epam.ignite.data.WebClients;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.stream.Stream;

/**
 * @author Dmitry
 * @since 22.01.2017
 */
public class IgniteDataImporter extends IgniteConnector {
    private static final Logger LOG =
            LoggerFactory.getLogger(IgniteDataImporter.class);


    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            LOG.error("Current number of parameters" + args.length);
            LOG.error("Usage: ignitedataimporter <in>");
            System.exit(2);
        }
        final File inputFile = new File(args[0]);
        final IgniteDataImporter dataImporter = new IgniteDataImporter();
        try (final Ignite ignite = dataImporter.getIgnite()) {
            //get or create cache
            final IgniteCache<String, IpData> ipDataCache =
                    ignite.getOrCreateCache(CACHE_NAME);
            final Stream<IpData> inputStream = Files.lines(Paths.get(inputFile.toURI()))
                    .map(StringParser::parseString2Data);

            inputStream.forEach(newData -> {
                final String key = newData.getIp();
                final Lock lock = ipDataCache.lock(key);
                lock.lock();
                try {
                    final IpData oldData = ipDataCache.get(key);
                    if (oldData == null) {
                        ipDataCache.put(key, newData);
                    } else {
                        final long newBytes = newData.getBytes() + oldData.getBytes();
                        newData.setBytes(newBytes);
                        final int newCounter = newData.getCounter() + oldData.getCounter();
                        newData.setCounter(newCounter);
                        final Set<WebClients> newClients = new HashSet<>(5);
                        newClients.addAll(newData.getClients());
                        newClients.addAll(oldData.getClients());
                        newData.setClients(newClients);
                        ipDataCache.put(key, newData);
                    }
                } finally {
                    lock.unlock();
                }
            });
        }
    }
}
