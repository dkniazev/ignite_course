package com.epam.ignite;

import com.epam.ignite.data.IpData;
import com.epam.ignite.data.WebClients;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.binary.BinaryObject;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.ScanQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.cache.Cache;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitrii_Kniazev
 * @since 01/23/2017
 */
public class IgniteDataReport extends IgniteConnector {
    private static final Logger LOG =
            LoggerFactory.getLogger(IgniteDataReport.class);

    public static void main(String[] args) {
        final IgniteDataReport dataImporter = new IgniteDataReport();
        try (final Ignite ignite = dataImporter.getIgnite()) {
            final IgniteCache<String, IpData> ipDataCache =
                    ignite.getOrCreateCache(CACHE_NAME).withKeepBinary();

            final ScanQuery<String, IpData> query = getQuery();
            try (final QueryCursor<Object> cursor =
                         ipDataCache.query(query, Cache.Entry::getValue)) {

                IpData minByte = new IpData();
                minByte.setBytes(Long.MAX_VALUE);
                IpData maxByte = new IpData();
                maxByte.setBytes(Long.MIN_VALUE);
                final Map<WebClients, Integer> clientMap = new HashMap<>(10);

                for (final Object obj : cursor) {
                    final IpData data = IpData.extractFromBinary((BinaryObject) obj);
                    System.out.println(BaseReport.ipData2String(data));
                    minByte = BaseReport.getMin(minByte, data);
                    maxByte = BaseReport.getMax(maxByte, data);
                    for (WebClients webClient : data.getClients()) {
                        final Integer num = clientMap
                                .compute(webClient, (k, v) -> v == null ? 0 : (v + 1));
                    }
                }
                System.out.println("\nMax bytes:\n" + BaseReport.ipData2String(maxByte));
                System.out.println("\nMin bytes:\n" + BaseReport.ipData2String(minByte));
                System.out.println();
                clientMap.forEach((k, v) -> System.out.printf("%s : %d\n", k, v));
            }
        }
    }

    private static ScanQuery<String, IpData> getQuery() {
        final ScanQuery<String, IpData> query = new ScanQuery<>();
        query.setPageSize(1000);
        return query;
    }
}
