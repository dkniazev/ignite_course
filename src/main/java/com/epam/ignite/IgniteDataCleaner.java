package com.epam.ignite;

import com.epam.ignite.data.IpData;
import com.epam.ignite.data.StringParser;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Dmitrii_Kniazev
 * @since 01/23/2017
 */
public class IgniteDataCleaner extends IgniteConnector {
    private static final Logger LOG =
            LoggerFactory.getLogger(IgniteDataCleaner.class);

    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            LOG.error("Current number of parameters" + args.length);
            LOG.error("Usage: ignitedataimporter <in>");
            System.exit(2);
        }
        final File inputFile = new File(args[0]);
        final IgniteDataCleaner dataCleaner = new IgniteDataCleaner();
        try (final Ignite ignite = dataCleaner.getIgnite()) {
            //get or create cache
            final IgniteCache<String, IpData> ipDataCache =
                    ignite.getOrCreateCache(CACHE_NAME);
            final Set<String> keys = Files.lines(Paths.get(inputFile.toURI()))
                    .map(StringParser::parseString2Data)
                    .map(IpData::getIp)
                    .collect(Collectors.toSet());
            keys.forEach(key -> {
                IpData data = ipDataCache.get(key);
                if (data == null) {
                    System.out.println("Key:" + key + "; NULL");
                } else {
                    System.out.println(data.toString());
                }
            });
            ipDataCache.clear();
        }
    }
}
