package com.epam.ignite;

import com.epam.ignite.data.IpData;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.configuration.TransactionConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.TransactionIsolation;

/**
 * @author Dmitrii_Kniazev
 * @since 01/23/2017
 */
public abstract class IgniteConnector {
    static final String CACHE_NAME = "ipDataCache";

    protected Ignite getIgnite() {
        final IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setClientMode(true);
        cfg.setPeerClassLoadingEnabled(true);
        //Discovery config
        final TcpDiscoverySpi discoverySpi = new TcpDiscoverySpi();
        discoverySpi.setForceServerMode(true);
        cfg.setDiscoverySpi(discoverySpi);
        //Cache config
        final CacheConfiguration<String, IpData> cacheCfg =
                new CacheConfiguration<>(CACHE_NAME);
        cacheCfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        cacheCfg.setIndexedTypes(String.class, IpData.class);
        cfg.setCacheConfiguration(cacheCfg);
        // Optional transaction configuration. Configure TM lookup here.
        final TransactionConfiguration txCfg = new TransactionConfiguration();
        txCfg.setDefaultTxConcurrency(TransactionConcurrency.OPTIMISTIC);
        txCfg.setDefaultTxIsolation(TransactionIsolation.SERIALIZABLE);
        cfg.setTransactionConfiguration(txCfg);

        return Ignition.start(cfg);
    }
}
